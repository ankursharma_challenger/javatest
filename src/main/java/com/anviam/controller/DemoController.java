package com.anviam.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.anviam.b.*;

@RestController
public class DemoController {

	@GetMapping("/methoda")
	public String methodA() {

		com.anviam.a.BeanFactoryWithApplicationContext aObject = new com.anviam.a.BeanFactoryWithApplicationContext();
		aObject.main();
		return "Calling methodA of Jar A";
	}

	@GetMapping("/methodb")
	public String methodB() {

		BeanFactoryWithApplicationContextB bObject = new BeanFactoryWithApplicationContextB();
		bObject.main();
		return "Calling methodB of Jar B";
	}
}