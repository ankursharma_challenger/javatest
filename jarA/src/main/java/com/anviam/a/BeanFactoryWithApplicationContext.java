package com.anviam.a;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanFactoryWithApplicationContext {
	public static void main(String... args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
		BeanFactory factory = (BeanFactory) context;
		TestA testA = (TestA) factory.getBean("testA");
		testA.methodA();
	}
}