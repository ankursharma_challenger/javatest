package com.anviam.b;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class BeanFactoryWithApplicationContextB {
	public static void main(String... args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("application-context1.xml");
		BeanFactory factory = (BeanFactory) context;
		TestB testB = (TestB) factory.getBean("testB");
		testB.methodB();
	}
}